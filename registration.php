<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <title>AD Code</title>
    <link rel="icon" type="image/png" href="./images/favicon.png">
    <link rel="stylesheet" href="./css/checkbox-svg.css">
    <link rel="stylesheet" href="build/css/intlTelInput.css">
    <?=$neogara?>
    <?=$metrika?>
</head>

<body>

    <div class="hover-modal"></div>
    <div
        style="display: inline-block;position: absolute;top: 4px;left: 50%;transform: translateX(-50%);font-size: .5em;opacity: .7;font-family: inherit;color:#e5e5e5;">
        -&nbsp;Advertorial&nbsp;-</div>
    <div id="popup_custom" class="popup_custom" style="display: none;">
        <div class="popup_overlay"></div>
        <a class="close_button">×</a>
        <div class="popup_inner">
            <div class="popup_content">
                <div class="popup_content_inner">
                    <div class="popup-content-wrapper">
                        <div class="popup-header">
                            <div class="title" id="blink">
                                You Just Made a BIG Mistake!
                            </div>
                            <div class="subtitle">
                                This is Your <b>LAST CHANCE</b> to Join The <b><i>AD Code</i></b> and secure your
                                financial future.
                            </div>
                        </div>
                    </div>
                    <div class="popup-form-wrapper">
                        <div class="form-container-unique">
                            <div class="form-block-2 whitee">
                                <form action="send.php" method="POST" id="bigForm2"
                                    class="intgrtn-form-signup gtm-popup-full-land neo_form" onsubmit="$('.preloader').show();">
                                    <?=$hiddens?>
                                    <div class="preloader"></div>
                                    <div class="form-group">
                                        <input type="text" id="first_name_pop" class="firstname form-control"
                                            name="firstname" required value="" placeholder="First name"
                                            aria-required="true">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="last_name_pop" name="lastname"
                                            class="lastname form-control" required value="" placeholder="Last name"
                                            aria-required="true">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="email_pop" name="email" required
                                            class="form-control email_cb email" value="<?=$_POST['email']?>"
                                            placeholder="Email" aria-required="true">
                                    </div>

                                    <div class="form-group last-row">
                                        <input type="tel"  name="phone_number" class="form-control phone js-phone" id="phone-popup"
                                            required placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="intgrtn-btn-submit submitBtn next"
                                            id="third_step_btn_pop" onclick="exitpage=false;">Start Now
                                        </button>
                                    </div>
                                    <div class="checkbox-svg">
                                        <input type="checkbox" id="cbx-2-pop" style="display: none;" checked="true">
                                        <label for="cbx-2-pop" class="checked-svg">
                                            <svg width="20px" height="20px" viewBox="0 0 18 18">
                                                <path
                                                    d="M1,9 L1,3.5 C1,2 2,1 3.5,1 L14.5,1 C16,1 17,2 17,3.5 L17,14.5 C17,16 16,17 14.5,17 L3.5,17 C2,17 1,16 1,14.5 L1,9 Z">
                                                </path>
                                                <polyline points="1 9 7 14 15 4"></polyline>
                                            </svg>
                                        </label>
                                        <div class="privacy-checkbox">
                                            <p>
                                                I agree to to the collection of my email address for the purposes of
                                                receiving commercial offers that we
                                                believe will be of interest to you on behalf of the companies and
                                                industries explicitly detailed in our
                                                <a href="./terms.html" target="_blank"> Terms & Conditions</a>
                                                and
                                                <a href="./privacy.html" target="_blank"> Privacy Policy.</a>
                                            </p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <header>
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <img src="./images/logo.png" class="img-fluid logo" />
                    </div>
                </div>
            </div>
            <div class="license-wrap">
                <div class="license-count">
                    <span class="count">7</span>
                </div>
                <div class="license-info">
                    <span class="license-left">Free Copies Left</span>
                    <span>Updated <span class="date">Today</span></span>
                </div>
            </div>
        </div>
    </header>
    <section>
        <div class="home-banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <span class="header-title">Guaranteed profits up to <b class="underline"><span
                                    class="currency">$</span>5,000 PER DAY</b> OR MORE STARTING TODAY</span>
                    </div>
                    <div class="col-md-12 col-lg-8">
                        <div class="videoWrapper">
                            <div class="up_sound">Enable sound</div>
                            <img src="./images/volume.png" id="volume_up">
                            <div class="anticlicker"></div>
                            <div id="ytplayer"></div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4 header-form">
                        <div class="signup-form">
                            <center><span style="color:red" id="error"></span></center>
                            <form action="send.php" method="POST" id="bigForm1"
                                class="intgrtn-form-signup gtm-main-full-land neo_form" onsubmit="$('.preloader').show();">
                                <?=$hiddens?>
                                <div class="preloader"></div>
                                <div class="form-group">
                                    <input type="text" id="first_name" class="firstname form-control" name="firstname"
                                        required="" value="" placeholder="First name" aria-required="true">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="last_name" name="lastname" class="lastname form-control"
                                        required value="" placeholder="Last name" aria-required="true">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="email" name="email" required
                                        class="form-control email_cb email" value="<?=$_POST['email']?>"
                                        placeholder="Email" aria-required="true">
                                </div>
                                
                                <div class="form-group last-row">
                                    <input type="tel" name="phone_number" class="form-control phone js-phone" id="phone" required
                                        placeholder="">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="intgrtn-btn-submit submitBtn next" id="third_step_btn"
                                        onclick="exitpage=false;">Start now
                                    </button>
                                </div>
                                <div class="checkbox-svg">
                                    <input type="checkbox" id="cbx" style="display: none;" checked="true">
                                    <label for="cbx" class="checked-svg">
                                        <svg width="20px" height="20px" viewBox="0 0 18 18">
                                            <path
                                                d="M1,9 L1,3.5 C1,2 2,1 3.5,1 L14.5,1 C16,1 17,2 17,3.5 L17,14.5 C17,16 16,17 14.5,17 L3.5,17 C2,17 1,16 1,14.5 L1,9 Z">
                                            </path>
                                            <polyline points="1 9 7 14 15 4"></polyline>
                                        </svg>
                                    </label>
                                    <div class="privacy-checkbox">
                                        <p>
                                            I agree to to the collection of my email address for the purposes of
                                            receiving commercial offers that we
                                            believe will be of interest to you on behalf of the companies and industries
                                            explicitly detailed in our
                                            <a href="./terms.html" target="_blank"> Terms & Conditions</a>
                                            and
                                            <a href="./privacy.html" target="_blank"> Privacy Policy.</a>
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="as-seen-on">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <img src="./images/partner-section.png" class="img-fluid seen-on-img" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="about-us">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <div class="about-img-wrap">
                            <img src="./images/about-me-new.png" class="img-fluid about-img" />
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="about-us-info">
                            <p class="title">Meet Rob <span class="goldman">Goldman</span></p>
                            <p class="pos">CEO & Founder, AD Code</p>
                            <div class="div-wrap">
                                <div class="divider"></div>
                            </div>
                            <p>AD Code is a premium online ad revenue system that provides members with passive income.
                                We
                                have just launched internationally and we’re looking for beta testers to make profits of
                                over</p>
                            <p class="earnings"><span class="currency">$</span>5000 a day</p>
                            <p class="p-video">Watch the video right now to learn more.</p>
                            <p class="sign">R <span class="goldman">Goldman</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="features">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="feature-info">
                            <i class="fa fa-file-text" aria-hidden="true"></i>
                            <p class="title">Step 1</p>
                            <p class="subtitle">Fill In The Form</p>
                            <p>When your registration is accepted you will automatically become a full member of the AD
                                Code
                                System</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-info">
                            <i class="fa fa-desktop" aria-hidden="true"></i>
                            <p class="title">Step 2</p>
                            <p class="subtitle">Activate Your Account</p>
                            <p>Next you need to activate your AD Code account, we will walk you through this simple
                                process</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-info">
                            <i class="fa fa-money" aria-hidden="true"></i>
                            <p class="title">Step 3</p>
                            <p class="subtitle">Watch Your Profits Grow</p>
                            <p>Follow all the instructions in the AD Code System and start to earn up to <span
                                    class="currency">$</span>5,000+ per day
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="bank-acc">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="title">My Bank Account Could Soon Be Yours...</p>
                        <p class="subtitle">In The Next 30 Days</p>
                        <div class="div-wrap">
                            <div class="divider"></div>
                        </div>
                        <img src="./images/Bank_USD.jpg" class="img-fluid img-bank_cur" style="width: 100%;" />
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p>Copyright &copy; <script>
                                document.write(new Date().getFullYear())
                            </script> All Rights Reserved</p>
                        <a target="_blank" href="disclaimer.html">Disclaimer</a> | <a target="_blank"
                            href="privacy.html">Privacy</a> | <a target="_blank" href="terms.html">Terms</a> | <a
                            target="_blank" href="abuse_report.html">Report&nbsp;Abuse/Spam</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="disclaimerAndText"
                            style="font-size:0.7em;color:inherit;padding:15px;border:1px solid inherit;margin-top: 15px;">
                            IMPORTANT: Earnings and Legal Disclaimers Earnings and income representations made by <span
                                class="disclaimer-brand_name__new">Website</span>, (collectively “This Website” only
                            used as aspirational
                            examples of your earnings potential. The success of those in the testimonials and other
                            examples are exceptional
                            results and therefore are not intended as a guarantee that you or others will achieve the
                            same results.
                            Individual results will vary and are entirely dependent on your use of <span
                                class="disclaimer-brand_name__new">Website</span>.
                            This Website is not responsible for your actions. You bear sole responsibility for your
                            actions and decisions
                            when using products and services and therefore you should always exercise caution and due
                            diligence. You agree
                            that this Website is not liable to you in any way for the results of using our products and
                            services. See our
                            Terms & Conditions for our full disclaimer of liability and other restrictions. This Website
                            may receive
                            compensation for products and services they recommend to you. If you do not want This
                            Website to be compensated
                            for a recommendation, then we advise that you search online for a similar product through a
                            non-affiliate link.
                            Trading can generate notable benefits, however, it also involves the risk of partial/full
                            loss of the invested
                            capital, therefore, you should consider whether you can afford to invest. ©<span
                                id="yearDisclaimerNew">2020</span><br />
                            USA REGULATION NOTICE: Trading Forex, CFDs and Cryptocurrencies is not regulated within the
                            United States.
                            Invest in Crypto is not supervised or regulated by any financial agencies nor US agencies.
                            Any unregulated
                            trading activity by U.S. residents is considered unlawful. <span
                                class="disclaimer-brand_name__new">Website</span> does not accept customers located
                            within the United States
                            or holding an American citizenship.
                            <script type="text/javascript">
                                var yearDisclaimerNew = new Date();
                                document.getElementById("yearDisclaimerNew").innerHTML = yearDisclaimerNew
                                .getFullYear();
                                document.querySelectorAll(".disclaimer-brand_name__new").forEach(function (brandName) {
                                    brandName.innerHTML = location.hostname;
                                })
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div class="floating-earnings show">
        <img src="./images/icon-image.png">
        <div class="earning-info">
            <span class="name"><span class="earner-name">Della just earned</span> <span class="earner-amount"><span
                        class="currency" style="display:inline">$</span>101</span></span>
            <span class="with">with AD Code</span>
        </div>
    </div>


    <div class="modal" id="finishPopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-body">
                        <div class="modal--reg--block-header">
                            <p class="lead"><b>Congratulations</b>, now you're one step closer to financial
                                independence.</p>
                            <div>
                                <img src="./images/test-broker.png" class="img-responsive brand-logo broker_image"
                                    alt="Brand Logo">
                            </div>
                        </div>
                        <div class="infomsg">
                            <!-- <a href="#submit" class="btn btn-default btn-lg visible-xs" style="margin-bottom: 20px;">
                            <span class="modal--reg--btn-countdown">1</span>ПОТОРОПИТЕСЬ! АКТИВИРУЙТЕ ВАШУ УЧЕТНУЮ ЗАПИСЬ</a> -->
                            <h3>Start earning in 3 easy steps</h3>
                            <div class="row modal--reg--block-text">
                                <div class="col-sm-4">
                                    <img src="./images/finish-pop-1.png" alt="First" class="img-responsive"
                                        src="./images/finish-pop-1.png">
                                    <h4><b>1.</b><br>Make an initial deposit</h4>
                                    <p>Make an initial deposit of <span class="currency">$</span>250 using your
                                        preferred payment method.</p>
                                </div>
                                <div class="col-sm-4">
                                    <img src="./images/finish-pop-2.png" alt="Second" class="img-responsive"
                                        src="./images/finish-pop-2.png">
                                    <h4><b>2.</b><br>Get a free call from the Expert Advisor</h4>
                                    <p>Get free professional advice from a financial expert.</p>
                                </div>
                                <div class="col-sm-4">
                                    <img src="./images/finish-pop-3.png" alt="Third" class="img-responsive"
                                        src="./images/finish-pop-3.png">
                                    <h4><b>3.</b><br>Start making money from home</h4>
                                    <p>Start trading right now and earn money from the Bitcoin price today.</p>
                                </div>
                            </div>
                            <a href="" onclick="exitpage = false" class="btn btn-default btn-lg btn_redir">HURRY UP!
                                ACTIVATE YOUR ACCOUNT</a>
                            <div class="modal--reg--footer-text">
                                <h4><span class="modal--reg--lightbulb-icon"></span>Did you know that?</h4>
                                <p>You can earn <span class="modal--reg--highlight">huge profits</span> with <b>an
                                        average return of 88%</b> for every successful trade you make.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        #bitcoin-widget {
            bottom: 105px !important;
        }
    </style>
    <link rel="stylesheet" href="./css/css.css">
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/main.css">

    <script src="./js/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js" defer></script>
    <script src="./js/device.min.js"></script>
    <script src="./js/custom.js" defer></script>
    <script src="./js/jquery.validate.min.js"></script>
    <script src="./js/getdetector.js"></script>
    <script src="./build/js/intlTelInput.min.js"></script>
    <script src="./js/unload.js"></script>
    <script src="./js/script.js"></script>

    <script>
    $("input[type=tel]").intlTelInput({
      autoFormat: true,
      autoPlaceholder: "aggressive",
      defaultCountry: "auto",
      geoIpLookup: function (callback) {
        $.get('//ipinfo.io', function () {}, "jsonp").always(
          function (
            resp) {
            var countryCode = (resp && resp.country) ? resp.country : "";
            callback(countryCode);
          });
      },
      nationalMode: false,
      hiddenInput: "phone",
      numberType: "MOBILE",
    });
  </script>
    <script src="./js/bitcoin-widget.js" id="widget-script" data-widget-cur="USD"></script>
    <?=$partner?>
</body>

</html>