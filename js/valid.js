$(document).ready(function(){

    // Custom method to validate username
    $.validator.addMethod("usernameRegex", function(value, element) {
        return this.optional(element) || regex_first_last_name.test(value);
    }, "Name must be more than 2 characters long, without special characters or spaces");
    $.validator.addMethod("lastusernameRegex", function(value, element) {
        return this.optional(element) || regex_first_last_name.test(value);
    }, "Last name must be more than 2 characters long, without special characters or spaces");
    $.validator.addMethod("passwordRegex", function(value, element) {
        return this.optional(element) || /[a-z]/.test(value) && /[0-9]/.test(value) && /[A-Z]/.test(value) && /^[0-9A-Za-z]+$/.test(value);
    }, 'The password field is required');
    $.validator.addMethod("phoneRegex", function(value, element) {
        return this.optional(element) || /^(\d[- ]?){7,11}$/.test(value);
    }, "The phone must be from 7 to 11 characters, without special characters");
    // $.validator.addMethod("emailRegex", function(value, element) {
    //     return this.optional(element) || /^[a-z0-9_.-]{2,15}@[a-z0-9_-]{2,15}.[a-z]{1,7}$/i.test(value);
    // }, "E-mail must be valid");

    $(function(){
        var form = $("#bigForm1")
        form.validate({
            onfocusout: function (element) {
                if(this.currentElements.length != 0 && this.currentElements[0].name == "email"){
                    rebuidEmail($(this.currentElements[0]))
                }
                this.element(element);
            },
            onkeyup: function(element) {
                $(element).valid()
            },

            rules: {
                first_name: {
                    required: true,
                    usernameRegex: true,
                    minlength: 2,
                    maxlength: 60,
                },
                last_name:{
                    required: true,
                    lastusernameRegex: true,
                    minlength: 2,
                    maxlength: 60,
                },
                password : {
                    required: true,
                    passwordRegex: true,
                    minlength: 8,
                    maxlength: 12,
                },
                email: {
                    required: true,
                    email: true,

                },
                phone:{
                    phoneRegex: true,
                    required: true,
                }



            },
            messages: {
                first_name:{
                    required: "The first name field is required",
                    minlength: "The first name must be at least 2 characters",
                    maxlength: "First name can be a maximum of 25 characters",
                },

                last_name:{
                    required: "The last name field is required",
                    minlength: "The last name must be at least 2 characters",
                    maxlength: "Surname can be a maximum of 25 characters",
                },
                password:{
                    required: "The password field is required",
                    minlength: "The password must be at least 8 characters",
                    maxlength: "The password may not be greater than 12 characters",
                },
                email:{
                    required: "The email field is required",
                    email: "The email must be a valid address",
                },
                phone:{
                    required: "The phone number field is required",
                }

            },
            submitHandler: function(form, event) {
                event.preventDefault();
                $('.preloader').show();
                $("input[name='first_name']").each(function(){
                    $(this).val($(this).val().substr(0,60).replace(/[.-]/g, ' ').replace(/\s\s+/g, ' '))
                });
                $("input[name='last_name']").each(function(){
                    $(this).val($(this).val().substr(0,60).replace(/[.-]/g, ' ').replace(/\s\s+/g, ' '))
                });
                var msg = $(form).serialize();
                var linkAdress = makeSendAdress();
                console.log('linkAdress= ' + linkAdress);

                $.post(linkAdress, msg)
                    .done(function(data){
                        
                        var domainForPixel = $.urlParam('domain');
                        if(domainForPixel != null){
                            $('body').prepend('<iframe width="1" height="1" alt="" style="display:none" src="https://' + decodeURIComponent(domainForPixel) + '"></iframe>');
                        }
                        var obj_data = JSON.parse(data)
                        adress_redir = obj_data.redirect;

                        var httpsImage = obj_data.image.replace(/\.[a-z]{2}\./,'.');
                        httpsImage = httpsImage.replace('http','https');
                        $('.broker_image').attr('src', httpsImage);
                        $('.btn_redir').attr('href', adress_redir);
                        $('#popup_custom, .hover-modal').hide();
                        $('#finishPopup').show();
                        setTimeout(function(){$('.preloader').hide();},2000)

                    }).fail(function(jqXHR, textStatus, errorThrown) {
                    
                    if(jqXHR.status == 400){
                        var obj_data = JSON.parse(jqXHR.responseText)
                        for(key in obj_data.errors){
                            if(key == "CROB"){
                                window.location = obj_data.errors[key]
                            }else{
                                alert(obj_data.errors[key])
                            }
                        }
                    }else {
                        alert( 'Register form field error' )
                        console.log(jqXHR)
                    }
                    $('.preloader').hide();
                    $('#personal_phone').hide(500);
                    $('#account_information').show(500);
                    $('.password').val('');
                });

            }
        });
       
    });

    $(function(){
        var form = $("#bigForm2")
        form.validate({
            onfocusout: function (element) {
                if(this.currentElements.length != 0 && this.currentElements[0].name == "email"){
                    rebuidEmail($(this.currentElements[0]))
                }
                this.element(element);
            },
            onkeyup: function(element) {
                $(element).valid()
            },

            rules: {
                first_name: {
                    required: true,
                    usernameRegex: true,
                    minlength: 2,
                    maxlength: 60,
                },
                last_name:{
                    required: true,
                    lastusernameRegex: true,
                    minlength: 2,
                    maxlength: 60,
                },
                password : {
                    required: true,
                    passwordRegex: true,
                    minlength: 8,
                    maxlength: 12,
                },
                email: {
                    required: true,
                    email: true,

                },
                phone:{
                    phoneRegex: true,
                    required: true,
                }


            },
            messages: {
                first_name:{
                    required: "The first name field is required",
                    minlength: "The first name must be at least 2 characters",
                    maxlength: "First name can be a maximum of 25 characters",
                },

                last_name:{
                    required: "The last name field is required",
                    minlength: "The last name must be at least 2 characters",
                    maxlength: "Surname can be a maximum of 25 characters",
                },
                password:{
                    required: "The password field is required",
                    minlength: "The password must be at least 8 characters",
                    maxlength: "The password may not be greater than 12 characters",
                },
                email:{
                    required: "The email field is required",
                    email: "The email must be a valid address",
                },
                phone:{
                    required: "The phone number field is required",
                }

            },
            submitHandler: function(form, event) {
                event.preventDefault();
                $('.preloader').show();
                $("input[name='first_name']").each(function(){
                    $(this).val($(this).val().substr(0,60).replace(/[.-]/g, ' ').replace(/\s\s+/g, ' '))
                });
                $("input[name='last_name']").each(function(){
                    $(this).val($(this).val().substr(0,60).replace(/[.-]/g, ' ').replace(/\s\s+/g, ' '))
                });
                var msg = $(form).serialize();
                var linkAdress = makeSendAdress();
                console.log('linkAdress= ' + linkAdress);

                $.post(linkAdress, msg)
                    .done(function(data){
                        
                        var domainForPixel = $.urlParam('domain');
                        if(domainForPixel != null){
                            $('body').prepend('<iframe width="1" height="1" alt="" style="display:none" src="https://' + decodeURIComponent(domainForPixel) + '"></iframe>');
                        }
                        var obj_data = JSON.parse(data)
                        adress_redir = obj_data.redirect;

                        var httpsImage = obj_data.image.replace(/\.[a-z]{2}\./,'.');
                        httpsImage = httpsImage.replace('http','https');
                        $('.broker_image').attr('src', httpsImage);
                        $('.btn_redir').attr('href', adress_redir);
                        $('#popup_custom, .hover-modal').hide();
                        $('#finishPopup').show();
                        setTimeout(function(){$('.preloader').hide();},2000)

                    }).fail(function(jqXHR, textStatus, errorThrown) {
                        
                    if(jqXHR.status == 400){
                        var obj_data = JSON.parse(jqXHR.responseText)
                        for(key in obj_data.errors){
                            if(key == "CROB"){
                                window.location = obj_data.errors[key]
                            }else{
                                alert(obj_data.errors[key])
                            }
                        }
                    }else {
                        alert( 'Register form field error' )
                        console.log(jqXHR)
                    }
                    $('.preloader').hide();
                    $('#personal_phone_pop').hide(500);
                    $('#account_information_pop').show(500);

                    $('.password').val('');
                });

            }
        });
       
    });
});