<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>A special offer for you!</title>
  <link href="./images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
  <link rel="stylesheet" tytpe="text/css" href="./css/comeback.css">
  <?=$metrika?>
</head>

  <div class="container_logo">
    <div class="container-fluid box_wrap">
      <div class="first_box">
        <div class="row">
          <div class="col-xs-12 first_box_margin">
            <img src="./images/logo.png" alt="" class="first_box_img_logo">
          </div>
          <div class="row">
            <div class="col-xs-12 ">
              <p class="first_box_p"><span class="first_box_p_white">want </span><span class="first_box_p_yellow"> <span
                    class="currency">$</span>1.000</span></p>
              <p><span class="first_box_p_white_2">in </span><span class="first_box_p_yellow_2">60 minutes?</span></p>
              <p class="first_box_p_white_3">Walk away now and miss out on <br> your chance to make</p>
              <p class="first_box_p_purple"> <span class="currency">$</span>1.000 in 60 minutes</p>
              <p class="first_box_p_white_4">All you have to do is get your <span
                  class="first_box_p_purple_2">FREE</span> software now</p>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <form action="registration.php<?=$utm_form?>" method="post" role="form" class="form_box gtm-comeback-short-preland"
                onsubmit="$('.preloader').show();">
                <?=$hiddens?>
                <div class="preloader"></div>
                <input type="email" class="form_input" name="email" placeholder="Enter your email" required>
                <button type="sumbit" class="form_button">get started now <div class="form_button_img"></div></button>
              </form>
              <p class="first_box_p_white_5">We respect your privacy and your information is 100% secure</p>

            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  <div class="footer">
    <div class="container_logo_footer">
      <div class="container-fluid box_wrap">
        <div class="row">
          <div class="col-xs-12">
            <div class="partners">
              <img src="./images/partners.png" alt="" class="footer_img">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container_logo_footer_2">
      <div class="container-fluid box_wrap">
        <div class="row">
          <div class="col-xs-12">
            <div class="warning">
              <p> IMPORTANT: Earnings and Legal Disclaimers Earnings and income representations made by <span
                  class="disclaimer-brand_name__new">Website</span>, (collectively “This Website” only used as
                aspirational
                examples of your earnings potential. The success of those in the testimonials and other examples are
                exceptional
                results and therefore are not intended as a guarantee that you or others will achieve the same results.
                Individual results will vary and are entirely dependent on your use of <span
                  class="disclaimer-brand_name__new">Website</span>.
                This Website is not responsible for your actions. You bear sole responsibility for your actions and
                decisions
                when using products and services and therefore you should always exercise caution and due diligence. You
                agree
                that this Website is not liable to you in any way for the results of using our products and services.
                See our
                Terms & Conditions for our full disclaimer of liability and other restrictions. This Website may receive
                compensation for products and services they recommend to you. If you do not want This Website to be
                compensated
                for a recommendation, then we advise that you search online for a similar product through a
                non-affiliate link.
                Trading can generate notable benefits, however, it also involves the risk of partial/full loss of the
                invested
                capital, therefore, you should consider whether you can afford to invest. ©<span
                  id="yearDisclaimerNew">2020</span><br />
                USA REGULATION NOTICE: Trading Forex, CFDs and Cryptocurrencies is not regulated within the United
                States.
                Invest in Crypto is not supervised or regulated by any financial agencies nor US agencies. Any
                unregulated
                trading activity by U.S. residents is considered unlawful. <span
                  class="disclaimer-brand_name__new">Website</span> does not accept customers located within the United
                States
                or holding an American citizenship.
                <script type="text/javascript">
                  var yearDisclaimerNew = new Date();
                  document.getElementById("yearDisclaimerNew").innerHTML = yearDisclaimerNew.getFullYear();
                  document.querySelectorAll(".disclaimer-brand_name__new").forEach(function (brandName) {
                    brandName.innerHTML = location.hostname;
                  })
                </script>
              </p>
            </div>

          </div>
          <div class="col-xs-12">
            <div class="footer_sticks">
              <p class="">
                <a href="./disclaimer.html" target="_blank" class="footer_link">DISCLAIMER </a>
                <a href="./terms.html" target="_blank" class="footer_link">Terms</a>
                <a href="./privacy.html" target="_blank" class="footer_link">Privacy</a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="./js/jquery.min.js"></script>
  <script src="./js/bootstrap.min.js"></script>
  <script src="./js/getdetector.js"></script>

  </body>

</html>