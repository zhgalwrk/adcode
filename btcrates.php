<?php
$api = file_get_contents("https://api.coindesk.com/v1/bpi/currentprice.json");
$api_json = json_decode($api, 1);

$ar = [
    'BTC' => [
        'EUR' => floating_num($api_json['bpi']['EUR']['rate_float']),
        'USD' => floating_num($api_json['bpi']['USD']['rate_float']),
        'GBP' => floating_num($api_json['bpi']['GBP']['rate_float'])
        ]
    ];
echo json_encode($ar);

function floating_num($num)
{
    $ar = explode(".", $num);
    $ar['1'] = substr($ar['1'] . '00', 0, 2);
    return implode(".", $ar);
}